In this code I wanted to show you my approach to design and development.
This code should perform cases provided in requirements + some extra from my understanding, i.e. '1 *' resolves to 1.
This project can be much better with unit tests and some more integration tests.
To test this properly requires some time, and I believe that you appreciate testability as it is now without any particular proof.

The points made here:
- logic independent from UI
- extensibility and testability
- a bit of OOP approach
- minimal boilerplate with SpringBoot and lombok 
- code quality in general

Calculator specs:

- *, /, -, + as main operations

- q to quit

- clr to clear

- all operations are binary, some values can be inferred ('1 \*' = '1')

- should accept numeric literals like 1e3, .3

- on division rounds to 20 digits after the point
