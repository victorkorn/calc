package victor.io.calc.cli;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import victor.io.calc.logic.Calculator;

import java.util.Scanner;

@Slf4j
@Component
@AllArgsConstructor
public class CommandLineLoop implements CommandLineRunner {

    private static final String QUIT_COMMAND = "q";
    private static final String CARET = "> ";

    private final Calculator calculator;

    @Override
    public void run(String... args) throws Exception {
        log.info("CommandLineLoop started");
        Scanner scanner = new Scanner(System.in);

        for (;;) {
            try {
                System.out.print(CARET);
                String line = scanner.nextLine();
                if (QUIT_COMMAND.equals(line)) {
                    break;
                }
                String message = calculator.feed(line);
                System.out.println(message);
            } catch (RuntimeException ex) {
                log.warn(ex.getMessage(), ex);
                break;
            }
        }
        log.info("CommandLineLoop terminated");
    }
}
