package victor.io.calc.logic;

import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.EmptyStackException;
import java.util.Stack;

@Component
public class CalculatorState {

    private Stack<BigDecimal> state = new Stack<>();

    public void clear() {
        state.clear();
    }

    public void push(String arg) {
        state.push(new BigDecimal(arg));
    }

    public void push(BigDecimal arg) {
        state.push(arg);
    }

    public BigDecimal pop() {
        return state.pop();
    }

    public BigDecimal popOrDefault(BigDecimal deflt) {
        try {
            return state.pop();
        } catch (EmptyStackException ex) {
            return deflt;
        }
    }
}
