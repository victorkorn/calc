package victor.io.calc.logic.ops;

import org.springframework.stereotype.Component;
import victor.io.calc.logic.CalculatorOperation;
import victor.io.calc.logic.CalculatorState;

import java.math.BigDecimal;

@Component
public class BiAddOperation implements CalculatorOperation {
    @Override
    public String opcode() {
        return "+";
    }

    @Override
    public String execute(CalculatorState state) {
        BigDecimal result = state.popOrDefault(BigDecimal.ZERO).add(state.popOrDefault(BigDecimal.ZERO));
        state.push(result);
        return result.toString();
    }
}
