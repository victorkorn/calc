package victor.io.calc.logic.ops;

import org.springframework.stereotype.Component;
import victor.io.calc.logic.CalculatorOperation;
import victor.io.calc.logic.CalculatorState;

import java.math.BigDecimal;

@Component
public class SubtractOperation implements CalculatorOperation {
    @Override
    public String opcode() {
        return "-";
    }

    @Override
    public String execute(CalculatorState state) {
        BigDecimal value = state.popOrDefault(BigDecimal.ZERO);
        BigDecimal result = state.popOrDefault(BigDecimal.ZERO).subtract(value);
        state.push(result);
        return result.toString();
    }
}
