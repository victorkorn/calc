package victor.io.calc.logic.ops;

import org.springframework.stereotype.Component;
import victor.io.calc.logic.CalculatorOperation;
import victor.io.calc.logic.CalculatorState;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Component
public class DivideOperation implements CalculatorOperation {

    private static final int PRECISION = 20;

    @Override
    public String opcode() {
        return "/";
    }

    @Override
    public String execute(CalculatorState state) {
        BigDecimal value = state.popOrDefault(BigDecimal.ZERO);
        BigDecimal result = state.popOrDefault(BigDecimal.ONE).divide(value, PRECISION, RoundingMode.HALF_UP);
        state.push(result);
        return result.toString();
    }
}
