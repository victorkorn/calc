package victor.io.calc.logic;

public interface CalculatorOperation {
    String opcode();
    String execute(CalculatorState state);
}
