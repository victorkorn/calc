package victor.io.calc.logic;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Slf4j
public class Calculator {

    private final CalculatorState state;
    private final Map<String, CalculatorOperation> operations;

    public Calculator(CalculatorState state, List<CalculatorOperation> ops) {
        operations = ops.stream().collect(Collectors.toMap(
                CalculatorOperation::opcode,
                Function.identity()
        ));
        this.state = state;
    }

    public String feed(String argline) {
        try {
            return feedInternal(argline);
        } catch (RuntimeException ex) {
            log.error(ex.getMessage(), ex);
            state.clear();
            if (ex instanceof ArithmeticException) {
                return "division by 0";
            }
            return "";
        }
    }

    private String feedInternal(String argline) {
        String[] args = argline.trim().split(" ");
        String op = args[args.length - 1];
        feedArgs(args);
        return Optional.ofNullable(operations.get(op))
                .map(it -> it.execute(state))
                .orElseGet(() -> {
                    state.push(args[0]);
                    return args[0];
                });
    }

    private void feedArgs(String[] args) {
        for (int i = 0; i < args.length - 1; i++) {
            state.push(args[i]);
        }
    }
}
