package victor.io.calc;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import victor.io.calc.cli.CommandLineLoop;
import victor.io.calc.logic.Calculator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class ExampleInOutIT {

    @MockBean
    private CommandLineLoop commandLineLoop;

    @Autowired
    private Calculator calculator;

    @Test
    public void example_1() {
        assertEquals("5", calculator.feed("5"));
        assertEquals("8", calculator.feed("8"));
        assertEquals("13", calculator.feed("+"));
    }

    @Test
    public void example_2() {
        assertEquals("13", calculator.feed("5 8 +"));
        assertEquals("0", calculator.feed("13 -"));
    }

    @Test
    public void example_3() {
        assertEquals("-3", calculator.feed("-3"));
        assertEquals("-2", calculator.feed("-2"));
        assertEquals("6", calculator.feed("*"));
        assertEquals("5", calculator.feed("5"));
        assertEquals("11", calculator.feed("+"));
    }

    @Test
    public void example_4() {
        assertEquals("5", calculator.feed("5"));
        assertEquals("9", calculator.feed("9"));
        assertEquals("1", calculator.feed("1"));
        assertEquals("8", calculator.feed("-"));
        assertTrue(calculator.feed("/").startsWith("0.625"));
    }
}
